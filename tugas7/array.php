<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Document</title>
</head>
<body>
      <h1>Contoh Array</h1>
      <?php
            echo "<h3>Soal 1</h3>";

            $kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"];
            $adults = ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];

            print_r($kids);
            print_r($adults);

            echo "<h3>Soal 2</h3>";

            echo "Total kids : " . count($kids). "<br>";
            echo "<ol>";
            echo "<li>" . $kids[0] . "</li>";
            echo "<li>" . $kids[1] . "</li>";
            echo "<li>" . $kids[2] . "</li>";
            echo "<li>" . $kids[3] . "</li>";
            echo "<li>" . $kids[4] . "</li>";
            echo "<li>" . $kids[5] . "</li>";
            echo "</ol>";

            echo "Total adults : " . count($adults). "</br>";
            echo "<ol>";
            echo "<li>" . $adults[0] . "</li>";
            echo "<li>" . $adults[1] . "</li>";
            echo "<li>" . $adults[2] . "</li>";
            echo "<li>" . $adults[3] . "</li>";
            echo "<li>" . $adults[4] . "</li>";
            echo "</ol>";

            echo "<h3> Soal 3 </h3>";

            $biodata = [
                  ["Name" => "Will Byers", "Age" => "12", "Aliases" => "Will the Wise", "Status" => "Alive"],
                  ["Name" => "Mike Wheeler", "Age" => "12", "Aliases" => "Dungeon Master", "Status" => "Alive"],
                  ["Name" => "Jim Hopper", "Age" => "43", "Aliases" => "Chief Hopper", "Status" => "Deceased"],
                  ["Name" => "Mike Eleven", "Age" => "12", "Aliases" => "El", "Status" => "Alive"],
            ];

            echo "<pre>";
            print_r($biodata);
            echo "</pre>";
            
            echo "Nama : " . $biodata[0]["Name"] . "<br>";
            echo "Age : " . $biodata[0]["Age"] . "<br>";
            echo "Aliases : " . $biodata[0]["Aliases"] . "<br>";
            echo "Status : " . $biodata[0]["Status"] . "<br>";

            echo "<br>";

            echo "Nama : " . $biodata[1]["Name"] . "<br>";
            echo "Age : " . $biodata[1]["Age"] . "<br>";
            echo "Aliases : " . $biodata[1]["Aliases"] . "<br>";
            echo "Status : " . $biodata[1]["Status"] . "<br>";

            echo "<br>";

            echo "Nama : " . $biodata[2]["Name"] . "<br>";
            echo "Age : " . $biodata[2]["Age"] . "<br>";
            echo "Aliases : " . $biodata[2]["Aliases"] . "<br>";
            echo "Status : " . $biodata[2]["Status"] . "<br>";

            echo "<br>";

            echo "Nama : " . $biodata[3]["Name"] . "<br>";
            echo "Age : " . $biodata[3]["Age"] . "<br>";
            echo "Aliases : " . $biodata[3]["Aliases"] . "<br>";
            echo "Status : " . $biodata[3]["Status"] . "<br>";

      ?>
</body>
</html>