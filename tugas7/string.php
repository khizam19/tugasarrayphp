<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Document</title>
</head>
<body>
      <h1>Contoh string</h1>

      <?php

            echo "<h3>soal pertama</h3>";
            
            $first_sentence = "Hello PHP!";
            echo "first_sentence : " . $first_sentence . "<br>";
            echo "panjang string : " . strlen($first_sentence) . "<br>";
            echo "jumlah kata : " . str_word_count($first_sentence) . "<br><br>";
            
            $second_sentence = "I'm ready for the challenges";
            echo "second_sentence : " . $second_sentence . "<br>";
            echo "panjang string : " . strlen($second_sentence) . "<br>";
            echo "jumlah kata : " . str_word_count($second_sentence) . "<br>" ;
            
            echo "<h3>soal kedua</h3>";

            $string2 = "I love PHP";
            echo "String : " . $string2 . "<br>";
            //echo "<label>String: </label> \"$string2\" <br>";
            echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
            echo "Kata kedua: " . substr($string2, 2, 5) ;
            echo "<br> Kata Ketiga: " . substr($string2, 6, 9) ;

            echo "<h3>soal ketiga</h3>";

            $string3 = "PHP is old but sexy!";
            echo "String3 : " . $string3 . "<br>";
            echo "Ganti kata sexy ke awesome : " . str_replace("sexy","awesome",$string3);

            
      ?>

</body>
</html>