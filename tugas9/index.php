<?php

require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');


$sheep = new Animal("shaun");

echo " Nama hewan : " . $sheep->hewan ."<br>";
echo " Kaki : " . $sheep->legs ."<br>";
echo " Berdarah dingin : " . $sheep->cold_blooded ."<br><br>";

$frog = new Frog("buduk");

echo " Nama hewan : " . $frog->hewan ."<br>";
echo " Kaki : " . $frog->legs ."<br>";
echo " Berdarah dingin : " . $frog->cold_blooded ."<br>";
echo " Jump : " . $frog->jump ."<br><br>";
// echo $frog->jump(2)
//$frog->jump(2);

$sungokong = new Ape("kera sakti");

echo " Nama hewan : " . $sungokong->hewan ."<br>";
echo " Kaki : " . $sungokong->legs ."<br>";
echo " Berdarah dingin : " . $sungokong->cold_blooded ."<br>";
echo " Jump : " . $sungokong->yell ."<br>";


?>